// 'use strict';
module.exports = function(sequelize, DataTypes) {
  var Pokemons = sequelize.define('Pokemons', {
    tipo: {
      type: DataTypes.STRING,
      validate: {
        isIn: [
          ['charizard', 'mewtwo', 'pikachu']
        ]
      }
    },
    treinador: DataTypes.STRING,
    nivel: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
  }, {
    schema: 'RENATOTEIXEIRA',
    timestamps: false
  });
  Pokemons.associate = function(models) {
    // associations can be defined here
  };
  return Pokemons;
};