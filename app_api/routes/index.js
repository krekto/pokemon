var express = require('express');
var router = express.Router();

var ctrlPokemons = require('../controllers/pokemons');

// pokemon
router.get('/pokemons', ctrlPokemons.pokemons);
router.post('/pokemons', ctrlPokemons.pokemonsCreate);
router.get('/pokemons/:pokemonid', ctrlPokemons.pokemonsReadOne);
router.put('/pokemons/:pokemonid', ctrlPokemons.pokemonsUpdateOne);
router.delete('/pokemons/:pokemonid', ctrlPokemons.pokemonsDeleteOne);

// pokemon battle
router.post('/pokemons/:pokemonAid/:pokemonBid', ctrlPokemons.pokemonsBattle);

module.exports = router;