//importa os modelos
var models = require('../models');

//resposta personalizada.
var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

//função para criar a lista de pokemons
var buildPokemonsList = function(result) {
  var pokemons = [];

  result.forEach(function(doc) {
    pokemons.push({
      id: doc.id,
      tipo: doc.tipo,
      treinador: doc.treinador,
      nivel: doc.nivel
    });
  });
  return pokemons;
};

var pokemonBattle = function(pokemonAid, pokemonBid, callback) {
  // o pokemon de nivel mais alto possui 2/3 de chance logo o outro 1/3
  var factorStrongLevelHigh = (2 / 3);
  var factorStrongLevelLow = (1 / 3);
  //gera numeros aleatorios para ser atribuidos a cada pokemon
  var factorRandomPokemonA = Math.random();
  var factorRandomPokemonB = Math.random();

  models.Pokemons.findAll({
    where: {
      id: [pokemonAid, pokemonBid]
    }
  }).then(function(foundPokemons) {
    var pokemonA, pokemonB;

    if (foundPokemons.length != 2) {
      console.log("pokemon not found");
      callback("notfound");
    } else {

      pokemons = buildPokemonsList(foundPokemons);

      pokemonA = pokemons[0];
      pokemonB = pokemons[1];

      if (pokemonA.nivel > pokemonB.nivel) {
        //que começe a batalha ...
        if (factorRandomPokemonA * factorStrongLevelHigh > factorRandomPokemonB * factorStrongLevelLow) {
          winnerLooserBattle = buildResultBattle(pokemonA, pokemonB, function(winnerLooserBattle) {
            callback(winnerLooserBattle);
          });

        } else {
          winnerLooserBattle = buildResultBattle(pokemonB, pokemonA, function(winnerLooserBattle) {
            callback(winnerLooserBattle);
          });
        }
      } else if (pokemonA.nivel < pokemonB.nivel) {
        //que começe a batalha ...
        if (factorRandomPokemonA * factorStrongLevelLow > factorRandomPokemonB * factorStrongLevelHigh) {
          winnerLooserBattle = buildResultBattle(pokemonA, pokemonB, function(winnerLooserBattle) {
            callback(winnerLooserBattle);
          });
        } else {
          winnerLooserBattle = buildResultBattle(pokemonB, pokemonA, function(winnerLooserBattle) {
            callback(winnerLooserBattle);
          });
        }
      } else if (pokemonA.nivel == pokemonB.nivel) {
        //que começe a batalha ...
        if (factorRandomPokemonA > factorRandomPokemonB) {
          winnerLooserBattle = buildResultBattle(pokemonA, pokemonB, function(winnerLooserBattle) {
            callback(winnerLooserBattle);
          });
        } else {
          winnerLooserBattle = buildResultBattle(pokemonB, pokemonA, function(winnerLooserBattle) {
            callback(winnerLooserBattle);
          });
        }
      }
    }
  });
};

var updatePokemonWinner = function(winner, callback) {
  //eleva o nivel vencedor +1
  models.Pokemons.update({
      nivel: winner.nivel + 1,
    }, {
      where: {
        id: winner.id
      }
    })
    .then(function() {

      models.Pokemons.findById(winner.id)
        .then(function(winner) {
          callback(winner.dataValues);
        }).catch(function(err) {
          console.log(err);
        });

    }).catch(function(err) {
      console.log(err);
    });
};

var updatePokemonLooser = function(looser, callback) {
  //eleva o nivel vencedor +1
  models.Pokemons.update({
      nivel: looser.nivel - 1,
    }, {
      where: {
        id: looser.id
      }
    })
    .then(function() {

      models.Pokemons.findById(looser.id)
        .then(function(looser) {
          callback(looser.dataValues);
          //se apos atualizar o perdedor o seu nivel ir a zedo = deletar
          if (looser.dataValues.nivel == 0) {
            models.Pokemons.destroy({
              where: {
                id: looser.id
              }
            }).then(function() {
              console.log(looser.treinador + "'s pokemon" + " died");
            }).catch(function(err) {
              console.log(err);
            });
          }
        }).catch(function(err) {
          console.log(err);
        });

    }).catch(function(err) {
      console.log(err);
    });
};

var buildResultBattle = function(winner, looser, callback) {
  var resultBattle = [];

  updatePokemonWinner(winner, function(updatedWinner) {

    updatePokemonLooser(looser, function(updatedLooser) {
      resultBattle.push({
        winner: updatedWinner,
        looser: updatedLooser
      });
      callback(resultBattle);
    });

  });
};

//lista os pokemons existentes
module.exports.pokemons = function(req, res) {
  var pokemons;

  // Lista pokemons
  models.Pokemons.findAll().then(function(result) {

    //função para criar a lista de pokemons
    pokemons = buildPokemonsList(result);
    sendJsonResponse(res, 200, pokemons);

  }).catch(function(err) {
    console.log(err);
    sendJsonResponse(res, 404, err);
  });
};

//cria um pokemon
module.exports.pokemonsCreate = function(req, res) {

  if (!req.body.tipo || !req.body.treinador) {
    sendJsonResponse(res, 400, {
      "message": "tipo and treinador are both required"
    });
    return;
  } else if (req.body.tipo != "charizard" && req.body.tipo != "mewtwo" && req.body.tipo != "pikachu") {

    sendJsonResponse(res, 400, {
      "message": "Type of pokemon needs to be pikachu, charizard or mewtwo"
    });
    return;

  } else {

    models.Pokemons.create({

      tipo: req.body.tipo,
      treinador: req.body.treinador

    }).then(function(pokemon) {
      pokemon.save();

      sendJsonResponse(res, 201, pokemon);

    }).catch(function(err) {
      // console.log(err);

      sendJsonResponse(res, 400, err);

    });

  }
};

//le um pokemon
module.exports.pokemonsReadOne = function(req, res) {
  // console.log(req.params.pokemonid);
  if (req.params && req.params.pokemonid) {

    models.Pokemons.findById(req.params.pokemonid).then(function(pokemon) {

      console.log(pokemon);

      if (!pokemon) {
        sendJsonResponse(res, 404, {
          message: "Pokemonid not found"
        });
        return;
      }

      sendJsonResponse(res, 200, pokemon.dataValues);

    }).catch(function(err) {
      sendJsonResponse(res, 404, err);
      return;
    });

  } else {
    sendJsonResponse(res, 404, {
      message: "No pokemonid in request"
    });
  }
};

//atualiza um pokemon
module.exports.pokemonsUpdateOne = function(req, res) {

  if (!req.params.pokemonid) {
    sendJsonResponse(res, 404, {
      "message": "Not found, pokemonid is required"
    });
    return;
  }

  models.Pokemons.update({
      treinador: req.body.treinador,
    }, {
      where: {
        id: req.params.pokemonid
      }
    })
    .then(function(result) {
      sendJsonResponse(res, 204, null);
    }).catch(function(err) {
      console.log(err);
    });
};

//deleta um pokemon
module.exports.pokemonsDeleteOne = function(req, res) {
  if (req.params.pokemonid) {
    models.Pokemons.destroy({
      where: {
        id: req.params.pokemonid
      }
    }).then(function() {
      sendJsonResponse(res, 204, null);
    }).catch(function(err) {
      console.log(err);
      sendJsonResponse(res, 404, err);
    });
  } else {
    sendJsonResponse(res, 404, {
      "message": "No pokemonid"
    });
  }
};

//Batalha de pokemons
module.exports.pokemonsBattle = function(req, res) {

  if (!req.params.pokemonAid || !req.params.pokemonBid) {
    sendJsonResponse(res, 404, {
      "message": "Not found, pokemonAid and pokemonBid is required"
    });
    return;
  }
  //função para realizar a batalha e retornar um array de objetos com winner e looser
  pokemonBattle(req.params.pokemonAid, req.params.pokemonBid, function(pokemonBattleresult) {
    if (pokemonBattleresult == "notfound") {
      sendJsonResponse(res, 404, {
        "message": "not found pokemon, sorry"
      });
    } else {
      sendJsonResponse(res, 200, pokemonBattleresult);
    }

  });
};