module.exports = {
  development: {
    username: 'DesafioAdmin',
    password: 'Picachu123',
    database: 'Desafio-jz',
    host: 'jzd-dev-desafio.database.windows.net',
    dialect: "mssql",
    //para o mssql azure precisa dessa opcao para o tedious
    dialectOptions: {
      encrypt: true
    }
  },
  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB,
    host: process.env.DB_HOST,
    dialect: "mssql",
    //para o mssql azure precisa dessa opcao para o tedious
    dialectOptions: {
      encrypt: true
    }
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB,
    host: process.env.DB_HOST,
    dialect: "mssql",
    //para o mssql azure precisa dessa opcao para o tedious
    dialectOptions: {
      encrypt: true
    }
  }
};