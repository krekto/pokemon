// get home page
module.exports.home = function(req, res) {
  res.render('index', {
    title: 'Pokemon'
  });
};