var express = require('express');
var router = express.Router();

var ctrlMain = require('../controllers/main');
var ctrlOthers = require('../controllers/others');

/* Locations pages */
router.get('/', ctrlMain.home);

/* Other pages */
router.get('/about', ctrlOthers.about);

module.exports = router;